package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/clientv3"
	"time"
)

func main() {
	var (
		config clientv3.Config
	)

	config = clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"},
		DialTimeout: 5 * time.Second, // 超时时间
	}

	// 建立连接
	client, err := clientv3.New(config)
	if err != nil {
		fmt.Println(err)
		return
	}

	// 用于读取etcd键值对
	kv := clientv3.NewKV(client)

	// 添加一个键值对
	resp, err := kv.Put(context.TODO(), "/cron/jobs/job3", "hello")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Revision (版本信息)", resp.Header.Revision)

	// 获取被覆盖前得值
	resp, err = kv.Put(context.TODO(), "/cron/jobs/job3", "bye", clientv3.WithPrevKV())
	if err != nil {
		fmt.Println(err)
		return
	}
	if resp.PrevKv != nil {
		fmt.Println("PrevKv (获取覆盖之前的值)", string(resp.PrevKv.Value))
	}

}
